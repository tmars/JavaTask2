package com.talipov.dao;

import com.talipov.model.TestResult;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями TestResult
 */
public class TestResultDao extends BaseDao<TestResult> {
    public TestResultDao() {
        super(TestResult.class);
    }
}
