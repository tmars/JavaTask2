package com.talipov.dao;

import com.talipov.model.Call;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями Call
 */
public class CallDao extends BaseDao<Call> {
    public CallDao() {
        super(Call.class);
    }
}
