package com.talipov.dao;

import com.talipov.model.Interview;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями Interview
 */
public class InterviewDao extends BaseDao<Interview> {
    public InterviewDao() {
        super(Interview.class);
    }
}
