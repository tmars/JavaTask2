package com.talipov.dao;

import com.talipov.model.InterviewAnswer;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями InterviewAnswer
 */
public class InterviewAnswerDao extends BaseDao<InterviewAnswer> {
    public InterviewAnswerDao() {
        super(InterviewAnswer.class);
    }
}