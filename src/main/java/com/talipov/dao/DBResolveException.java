package com.talipov.dao;

/**
 * Created by Марсель on 22.02.2017.
 * Исключение возникающее при связывании сущностей с базой данных
 */
public class DBResolveException extends Exception {
    public DBResolveException() {
    }

    public DBResolveException(String message) {
        super(message);
    }
}
