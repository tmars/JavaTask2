package com.talipov.dao;

import com.talipov.model.CallReason;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями CallReason
 */
public class CallReasonDao extends BaseDao<CallReason> {
    public CallReasonDao() {
        super(CallReason.class);
    }
}

