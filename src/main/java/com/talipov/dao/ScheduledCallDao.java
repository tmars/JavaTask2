package com.talipov.dao;

import com.talipov.model.ScheduledCall;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями ScheduledCall
 */
public class ScheduledCallDao extends BaseDao<ScheduledCall> {
    public ScheduledCallDao() {
        super(ScheduledCall.class);
    }
}
