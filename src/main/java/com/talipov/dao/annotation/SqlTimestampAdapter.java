package com.talipov.dao.annotation;

import javax.xml.bind.annotation.adapters.*;


/**
 * Created by Марсель on 22.02.2017.
 */
public class SqlTimestampAdapter extends XmlAdapter<java.util.Date, java.sql.Timestamp> {

    @Override
    public java.util.Date marshal(java.sql.Timestamp sqlDate) throws Exception {
        if(null == sqlDate) {
            return null;
        }
        return new java.util.Date(sqlDate.getTime());
    }

    @Override
    public java.sql.Timestamp unmarshal(java.util.Date utilDate) throws Exception {
        if(null == utilDate) {
            return null;
        }
        return new java.sql.Timestamp(utilDate.getTime());
    }

}