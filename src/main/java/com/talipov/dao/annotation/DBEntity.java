package com.talipov.dao.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Марсель on 21.02.2017.
 * Аннотация для указания сущности, которая тянется из бд
 */
@Retention(RUNTIME)
@Target({TYPE})
public @interface DBEntity {

    /**
     * Название таблицы, по умолчанию генерируется из названия класса
     */
    String name() default "";

    /**
     * Название колонки с идентификатором
     */
    String id() default "id";
}