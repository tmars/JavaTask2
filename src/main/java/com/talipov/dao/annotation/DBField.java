package com.talipov.dao.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Марсель on 21.02.2017.
 * Аннотация дла указания колонки из БД
 */
@Retention(RUNTIME) @Target({FIELD, METHOD})
public @interface DBField {

    /**
     * Названия колонки, по умолчанию генерируется по имени метода
     */
    String name() default "";
}