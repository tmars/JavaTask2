package com.talipov.dao.annotation;

import com.talipov.dao.BaseDao;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * Created by Марсель on 21.02.2017.
 * Аннотация для указания связи один ко многим
 */
@Retention(RUNTIME) @Target({FIELD, METHOD})
public @interface DBRelation {

    /**
     * Указывается контроллер по которому тянутся связанные объекты
     */
    Class<? extends BaseDao> controller();

    /**
     * Название метода, по которому определяется родитель
     */
    String methodName() default "";
}