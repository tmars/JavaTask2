package com.talipov.dao;

import com.talipov.connector.Connector;
import org.apache.log4j.Logger;
import com.talipov.model.Model;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.DBRelation;
import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.meta.MetaField;
import com.talipov.dao.meta.MetaRelation;
import org.apache.log4j.PropertyConfigurator;
import com.google.common.base.CaseFormat;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.*;
import java.util.*;

/**
 * Created by Марсель on 21.02.2017.
 * Базовый класс для свящывания объектов с базой данных
 */
public abstract class BaseDao<E extends Model> {

    /**
     * Логгер
     */
    private static final Logger logger = Logger.getLogger(BaseDao.class);

    /**
     * Соединение с базой данных
     */
    private Connection connection;

    /**
     * Класс сущности которая связана с таблицей
     */
    private Class<E> cls;

    /**
     * Список данных о полях сущности, которые необходимо связывать
     */
    private List<MetaField> fields = new ArrayList<>();

    /**
     * Список связей один ко многим с другими сущностиями
     */
    private List<MetaRelation> relations = new ArrayList<>();

    /**
     * Название таблицы
     */
    private String tableName = "";

    /**
     * Название колонки с id
     */
    private String idColumnName;

    /**
     * Список всех полей таблицы через запятую
     */
    private String columnNames;

    /**
     * Конструктор
     * @param cls класс сущности которая связана с таблицей
     */
    public BaseDao(Class<E> cls)  {
        PropertyConfigurator.configure("src/main/resources/log4j.xml");

        this.cls = cls;

        DBEntity dbEntity = cls.getAnnotation(DBEntity.class);
        this.idColumnName = dbEntity.id();
        this.tableName = dbEntity.name().equals("")
                ? CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, cls.getSimpleName())
                : dbEntity.name();

        ArrayList<String> columnNamesList = new ArrayList<>();

        for (Method method: cls.getMethods()){
            if (method.isAnnotationPresent(DBField.class)) {
                DBField dbField = method.getAnnotation(DBField.class);
                String mainName = method.getName().substring(3);
                MetaField field = new MetaField();

                field.type = method.getName().startsWith("get")
                        ? method.getReturnType()
                        : method.getParameters()[0].getType();
                field.columnName = dbField.name().equals("")
                        ? CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, mainName)
                        : dbField.name();

                try {
                    field.getterMethod = this.cls.getMethod("get" + mainName);
                } catch (NoSuchMethodException e) {
                    logger.error("Не создан " + this.cls.getSimpleName() + ".get" + mainName);
                    continue;
                }

                try {
                    field.setterMethod = this.cls.getMethod("set" + mainName, field.type);
                } catch (NoSuchMethodException e) {
                    logger.error("Не создан " + this.cls.getSimpleName() + ".set" + mainName);
                    continue;
                }

                fields.add(field);
                columnNamesList.add(field.columnName);

            } else if (method.isAnnotationPresent(DBRelation.class)) {
                DBRelation dbRelation = method.getAnnotation(DBRelation.class);
                String mainName = method.getName().substring(3);
                MetaRelation relation = new MetaRelation();

                relation.controller = dbRelation.controller();
                relation.columnName = this.tableName + "_id";
                relation.revertMethodName = dbRelation.methodName().equals("")
                        ? CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, relation.columnName)
                        : dbRelation.methodName();

                try {
                    relation.getterMethod = this.cls.getMethod("get" + mainName);
                } catch (NoSuchMethodException e) {
                    logger.error("Не созданы getter get"+mainName);
                    continue;
                }

                relations.add(relation);
            }
        }

        columnNames = String.join(",", columnNamesList);
    }

    /**
     * Список всех сущностей из базы данных
     * @return коллекцию объектов E
     */
    public Collection<E> getAll() throws SQLException, DBResolveException {
        return getList("SELECT " + idColumnName + "," + columnNames + " FROM \"" + tableName + "\"");
    }

    /**
     * Список всех сущностей из базы данных, попадающий под условию
     * @param where условие запроса
     * @return коллекцию объектов E
     */
    public Collection<E> getAllWhere(String where) throws SQLException, DBResolveException {
        return getList("SELECT " + idColumnName + "," + columnNames + " FROM \"" + tableName + "\" WHERE " + where);
    }

    /**
     * По sql запорсу возвращает список сущностей из базы данных
     * @param sql запрос
     * @return коллекцию объектов E
     */
    private Collection<E> getList(String sql) throws SQLException, DBResolveException {
        HashMap<Long, E> list = new HashMap<>();

        PreparedStatement ps = getConnection().prepareStatement(sql);
        ResultSet rs;
        try {
            rs = ps.executeQuery();
        } catch (SQLException e) {
            throw new SQLException("Ошибка при обработке запроса: \"" + sql + "\"" + ": " + e.getMessage());
        }

        while (rs.next()) {
            E object = pullEntity(rs);
            list.put(object.getId(), object);
        }
        ps.close();

        if (relations.size() > 0) {
            ArrayList<String> idsList = new ArrayList<>(list.size());
            for (Long id: list.keySet()) {
                idsList.add(Long.toString(id));
            }

            for (MetaRelation relation: relations) {
                BaseDao controller = null;
                try {
                    controller = relation.controller.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    logger.error("Не удалось создать объект связанного типа");
                    throw new DBResolveException();
                }

                String where = relation.columnName + " in (" + String.join(",", idsList) + ")";
                Collection<? extends Model> childs = controller.getAllWhere(where);

                try {
                    for (Model child : childs) {
                        Long parentId = (Long) controller.cls.getMethod("get" + relation.revertMethodName).invoke(child);
                        List cs = (List) relation.getterMethod.invoke(list.get(parentId));
                        cs.add(child);
                    }
                } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException e) {
                    logger.error("Ошибка при вызове метода связанной сущности "
                        + controller.getEntityClass().getSimpleName() + "."
                        + "get" + relation.revertMethodName);
                    throw new DBResolveException();
                }
            }
        }

        return list.values();
    }

    /**
     * Записываел объект в базу данных и устанавливает ей id
     * @param entity объект
     * @throws SQLException ошибка при обработке запроса
     */
    public void insert(Model entity) throws SQLException, DBResolveException {
        if (entity.getClass() != this.cls) {
            throw new DBResolveException("Неверный тип сущности. Ожидается: " + this.cls.getSimpleName()
                + ". Передан: " + entity.getClass().getSimpleName());
        }

        String sql = "INSERT INTO \"" + tableName + "\" (" + idColumnName + "," + columnNames + ") " +
                "VALUES(" + String.join(",", Collections.nCopies(fields.size()+1, "?")) + ")";

        PreparedStatement ps = getConnection().prepareStatement(sql);
        try {
            pushEntity(ps, entity);
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLException("Ошибка при обработке запроса: \"" + sql + "\"" + ": " + e.getMessage());
        }
    }

    /**
     * Возвращает класс сущности
     * @return
     */
    public Class getEntityClass() {
        return this.cls;
    }

    /**
     * Вставляет в запрос параметры - данные из сущности
     * @param ps запрос
     * @param entity сущность
     */
    private void pushEntity(PreparedStatement ps, Model entity) throws SQLException, DBResolveException {
        int i = 1;

        ps.setLong(i++, entity.getId());

        for (MetaField metaField: fields) {
            Object value = null;
            try {
                value = metaField.getterMethod.invoke(entity);
            } catch (InvocationTargetException | IllegalAccessException e) {
                logger.error("Ошибка доступа к аксессору");
                throw new DBResolveException();
            }

            switch (metaField.type.getSimpleName()) {
                case "long":
                    ps.setLong(i, (long) value);
                    break;
                case "Timestamp":
                    ps.setTimestamp(i, (Timestamp) value);
                    break;
                case "String":
                    ps.setString(i, (String) value);
                    break;
                case "Date":
                    ps.setDate(i, (java.sql.Date) value);
                    break;
                default:
                    logger.error("Не указан обработчик вставки для типа " + metaField.type.getSimpleName());
                    throw new DBResolveException();
            }

            i++;
        }
    }

    /**
     * Вытягивает данные из результата запроса и создает сущность
     * @param rs результат запроса
     * @return сущность
     */
    private E pullEntity(ResultSet rs) throws SQLException , DBResolveException {
        E object = null;

        try {
            object = this.cls.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            logger.error("Ошибка при создании сущности");
            throw new DBResolveException();
        }

        object.setId(rs.getLong(idColumnName));

        try {
            for (MetaField metaField: fields) {
                switch (metaField.type.getSimpleName()) {
                    case "long":
                        metaField.setterMethod.invoke(object, rs.getLong(metaField.columnName));
                        break;
                    case "Timestamp":
                        metaField.setterMethod.invoke(object, rs.getTimestamp(metaField.columnName));
                        break;
                    case "String":
                        metaField.setterMethod.invoke(object, rs.getString(metaField.columnName));
                        break;
                    case "Date":
                        metaField.setterMethod.invoke(object, rs.getDate(metaField.columnName));
                        break;
                    default:
                        logger.error("Не указан обработчик вставки для типа " + metaField.type.getSimpleName());
                        throw new DBResolveException();
                }
            }
        } catch (InvocationTargetException | IllegalAccessException e) {
            logger.error("Ошибка при инициализации сущности");
            throw new DBResolveException();
        }

        return object;
    }

    /**
     * Преобразовывает строку с названием из CamelCase в lower_case
     * @param camelCase входная строка
     * @return lower_case
     */
    private String getLowerCaseName(String camelCase) {
        return camelCase.replaceAll("(.)(\\p{Upper})", "$1_$2").toLowerCase();
    }

    private Connection getConnection() throws SQLException {
        if (connection == null) {
            connection = Connector.getConnection();
        }

        return connection;
    }
}
