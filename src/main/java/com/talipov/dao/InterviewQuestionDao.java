package com.talipov.dao;

import com.talipov.model.InterviewQuestion;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями InterviewQuestion
 */
public class InterviewQuestionDao extends BaseDao<InterviewQuestion> {
    public InterviewQuestionDao() {
        super(InterviewQuestion.class);
    }
}
