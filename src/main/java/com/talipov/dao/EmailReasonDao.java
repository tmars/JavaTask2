package com.talipov.dao;

import com.talipov.model.EmailReason;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями EmailReason
 */
public class EmailReasonDao extends BaseDao<EmailReason> {
    public EmailReasonDao() {
        super(EmailReason.class);
    }
}