package com.talipov.dao;

import com.talipov.model.Email;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями Email
 */
public class EmailDao extends BaseDao<Email> {
    public EmailDao() {
        super(Email.class);
    }
}
