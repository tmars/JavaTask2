package com.talipov.dao;

import com.talipov.model.Superuser;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями Superuser
 */
public class SuperuserDao extends BaseDao<Superuser> {
    public SuperuserDao() {
        super(Superuser.class);
    }
}
