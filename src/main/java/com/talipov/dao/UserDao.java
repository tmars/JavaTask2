package com.talipov.dao;

import com.talipov.model.User;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями User
 */
public class UserDao extends BaseDao<User> {
    public UserDao() {
        super(User.class);
    }
}
