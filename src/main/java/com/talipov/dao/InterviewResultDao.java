package com.talipov.dao;

import com.talipov.model.InterviewResult;

import java.sql.SQLException;

/**
 * Created by Марсель on 21.02.2017.
 * DAO для работы с сущностями InterviewResult
 */
public class InterviewResultDao extends BaseDao<InterviewResult> {
    public InterviewResultDao() {
        super(InterviewResult.class);
    }
}
