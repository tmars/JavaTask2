package com.talipov.dao.meta;

import java.lang.reflect.Method;

/**
 * Created by Марсель on 22.02.2017.
 * Для хранения информации о поле сущности
 */
public class MetaField {
    public Method getterMethod;
    public Method setterMethod;
    public String columnName;
    public Class type;
}
