package com.talipov.dao.meta;

import com.talipov.dao.BaseDao;

import java.lang.reflect.Method;

/**
 * Created by Марсель on 22.02.2017.
 * Для хранения информации о связи между сущностями один ко многим
 */
public class MetaRelation {
    public Class<? extends BaseDao> controller;
    public String columnName;
    public Method getterMethod;
    public String revertMethodName;
}
