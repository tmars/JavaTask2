package com.talipov;

import com.talipov.connector.Connector;
import com.talipov.connector.DataImporter;
import com.talipov.dao.*;
import com.talipov.model.*;
import com.talipov.serialize.XMLSerializer;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.xml.crypto.Data;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;

public class Main {

    /**
     * Логгер
     */
    private static final Logger logger = Logger.getLogger(Main.class);

    static {
        PropertyConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void main(String[] args) {
        BaseDao[] daos = new BaseDao[] {
                new CallDao(),
                new CallReasonDao(),
                new EmailDao(),
                new EmailReasonDao(),
                new InterviewDao(),
                new InterviewAnswerDao(),
                new InterviewQuestionDao(),
                new InterviewResultDao(),
                new ScheduledCallDao(),
                new SuperuserDao(),
                new TestResultDao(),
                new UserDao()
        };

//        serialize(daos);
//        deserialize(daos);

        importToDB();
    }

    public static void serialize(BaseDao[] daos) {
        logger.trace("Запускаем сериализацию.");

        ExecutorService pool = Executors.newCachedThreadPool();
        for (BaseDao dao: daos) {
            pool.submit(() -> {
                Logger logger = Logger.getLogger(dao.getClass());

                Collection objects;
                try {
                    objects = dao.getAll();
                } catch (SQLException e) {
                    logger.error("Ошибка при извлечении из БД", e);
                    return;
                } catch (DBResolveException e) {
                    logger.error("Ошибка заполнения сущностей", e);
                    return;
                }

                XMLSerializer.serialize(dao.getEntityClass(), objects, getFilename(dao.getClass()));
                logger.trace("Сериализация " + dao.getEntityClass().getSimpleName() +
                        " закончена. Сериализовано объектов: " + objects.size());
            });
        }

        pool.shutdown();
        try {
            pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error("Ошибка ожидания потоков.");
        }

        logger.trace("Завершено.");
    }

    public static void deserialize(BaseDao[] daos) {
        logger.trace("Запускаем десериализацию.");

        ExecutorService pool = Executors.newCachedThreadPool();
        for (BaseDao dao: daos) {
            pool.submit(() -> {
                Logger logger = Logger.getLogger(dao.getClass());

                Collection objects = XMLSerializer.deserialize(dao.getEntityClass(), getFilename(dao.getClass()));
                logger.trace("Деериализация " + dao.getEntityClass().getSimpleName() +
                        " закончена. Получено объектов: " + objects.size());
            });
        }

        pool.shutdown();
        try {
            pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error("Ошибка ожидания потоков.");
        }

        logger.trace("Завершено.");
    }

    public static void importToDB() {
        logger.trace("Начало иморта в БД");

        try {
            if (Connector.truncateAllTables()) {
                logger.trace("Таблицы очищены");
            }
        } catch (SQLException e) {
            logger.error("Ошибка сброса таблиц", e);
        }

        DataImporter.importObjects(new UserDao(), getFilename(UserDao.class));
        DataImporter.importObjects(new SuperuserDao(), getFilename(SuperuserDao.class));
        DataImporter.importObjects(new InterviewDao(), getFilename(InterviewDao.class));
        DataImporter.importObjects(new CallReasonDao(), getFilename(CallReasonDao.class));
        DataImporter.importObjects(new EmailReasonDao(), getFilename(EmailReasonDao.class));
        DataImporter.waitImport();

        DataImporter.importObjects(new TestResultDao(), getFilename(TestResultDao.class));
        DataImporter.importObjects(new InterviewQuestionDao(), getFilename(InterviewQuestionDao.class));
        DataImporter.importObjects(new InterviewResultDao(), getFilename(InterviewResultDao.class));
        DataImporter.waitImport();

        DataImporter.importObjects(new EmailDao(), getFilename(EmailDao.class));
        DataImporter.importObjects(new CallDao(), getFilename(CallDao.class));
        DataImporter.importObjects(new InterviewAnswerDao(), getFilename(InterviewAnswerDao.class));
        DataImporter.waitImport();

        DataImporter.importObjects(new ScheduledCallDao(), getFilename(ScheduledCallDao.class));
        DataImporter.waitImport();

        DataImporter.finish();

        logger.trace("Завершено.");
    }



    public static String getFilename(Class daoClass) {
        return "src/main/resources/data/" + daoClass.getSimpleName() + ".xml";
    }
}
