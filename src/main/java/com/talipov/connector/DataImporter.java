package com.talipov.connector;

import com.talipov.Main;
import com.talipov.dao.BaseDao;
import com.talipov.dao.DBResolveException;
import com.talipov.model.Model;
import com.talipov.serialize.XMLSerializer;
import org.apache.log4j.Logger;

import java.sql.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;

/**
 * Created by Марсель on 23.02.2017.
 * Класс который импортирует в БД
 */
public class DataImporter {

    /**
     * Логгер
     */
    private static final Logger logger = Logger.getLogger(DataImporter.class);

    /**
     * Пул потоков для имопрта данных
     */
    private static ExecutorService pool = Executors.newCachedThreadPool();

    /**
     * Список futures для ожидания ответов
     */
    private static ArrayList<Future> futures = new ArrayList<>();

    /**
     * Иморт данных в БД
     * @param dao
     */
    public static void importObjects(BaseDao dao, String filename) {
        Collection<Model> objects = XMLSerializer.deserialize(dao.getEntityClass(), filename);

        Future future = pool.submit(() -> {
            try {
                for (Model object: objects) {
                    dao.insert(object);
                }
            } catch (SQLException e) {
                logger.error("Ошибка импорта в БД", e);
            } catch (DBResolveException e) {
                logger.error("Ошибка заполнения сущностей", e);
            }

            logger.trace("Загрузка данных " + dao.getEntityClass().getSimpleName() +
                    " закончена. Загрузка объектов: " + objects.size());
        });
        futures.add(future);
    }

    /**
     * Ожидает завершения работы активных потоков
     */
    public static void waitImport() {
        try {
            for (Future future: futures) {
                future.get();
            }
        } catch (InterruptedException | ExecutionException e) {
            logger.error("Ошибка ожидания.", e);
        }
    }

    public static void finish() {
        pool.shutdown();
        try {
            pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            logger.error("Ошибка ожидания потоков.");
        }
    }

}
