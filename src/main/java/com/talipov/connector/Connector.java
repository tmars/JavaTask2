package com.talipov.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;


/**
 * Created by Марсель on 23.02.2017.
 * Для подключения к БД
 */
public class Connector {

    private static final String DB_HOST = "jdbc:postgresql://localhost:5432/stc";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123";


    /**
     * Возвращает соединение для работы с БД
     * @return Connection
     * @throws SQLException если не удалось
     */
    public static Connection getConnection() throws SQLException {
        Properties p = new Properties();
        p.put("user", DB_USER);
        p.put("password", DB_PASSWORD);
        p.put("characterEncoding", "UTF-8");
        return DriverManager.getConnection(DB_HOST, p);
    }


    /**
     * Очищает данные из всех таблиц
     * @return результат работы
     * @throws SQLException если не удалось
     */
    public static boolean truncateAllTables() throws SQLException {
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        boolean result = statement.execute("SELECT truncate_tables('postgres');");
        connection.close();
        statement.close();

        return result;
    }

}
