package com.talipov.serialize;

import com.google.common.base.CaseFormat;
import com.sun.xml.internal.txw2.output.IndentingXMLStreamWriter;
import com.talipov.dao.BaseDao;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import javax.xml.bind.*;
import javax.xml.stream.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Марсель on 22.02.2017.
 */
public class XMLSerializer {

    /**
     * Логгер
     */
    private static final Logger logger = Logger.getLogger(BaseDao.class);

    static {
        PropertyConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void serialize(Class entityClass, Collection objects, String filename) {
        File file = new File(filename);
        FileOutputStream stream;
        try {
            stream = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            logger.error("Ошибка записи файла", e);
            return;
        }

        JAXBContext jc;
        Marshaller m;
        try {
            jc = JAXBContext.newInstance(entityClass);
            m = jc.createMarshaller();
            m.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        } catch (JAXBException e) {
            logger.error("Ошибка создания JAXB", e);
            return;
        }

        try {
            XMLStreamWriter writer = new IndentingXMLStreamWriter(
                XMLOutputFactory.newFactory().createXMLStreamWriter(stream)
            );
            writer.setDefaultNamespace("http://www.namespace.com");
            writer.writeStartDocument();
            writer.writeStartElement(entityClass.getSimpleName());
            for (Object object: objects) {
                m.marshal(object, writer);
            }
            writer.writeEndDocument();
            writer.close();
        } catch (XMLStreamException e) {
            logger.error("Ошибка формирования XML файла", e);
        } catch (JAXBException e) {
            logger.error("Ошибка работы с JAXB", e);
        }
    }

    public static Collection deserialize(Class entityClass, String filename) {
        ArrayList<Object> list = new ArrayList<>();

        File file = new File(filename);
        FileInputStream stream;
        try {
            stream = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            logger.error("Ошибка чтения файла", e);
            return list;
        }

        Unmarshaller m = null;
        try {
            JAXBContext jc = JAXBContext.newInstance(entityClass);
            m = jc.createUnmarshaller();
        } catch (JAXBException e) {
            logger.error("Ошибка создания JAXB", e);
            return list;
        }

        XMLStreamReader reader = null;
        try {
            reader = XMLInputFactory.newFactory().createXMLStreamReader(stream);
            reader.next();
            reader.next();
            reader.next();

            while(reader.hasNext()) {
                list.add(m.unmarshal(reader));
                reader.next();
            }
        } catch (XMLStreamException e) {
            logger.error("Ошибка чтения XML файла", e);
        } catch (JAXBException e) {
            logger.error("Ошибка работы с JAXB", e);
        }

        return list;
    }
}
