package com.talipov.model;

import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.SqlTimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class Email extends Model {

	private long userId;
	private long superuserId;
	private long emailReasonId;
	private java.sql.Timestamp sendedAt;
	private String content;
	private String subject;

	public long getUserId() {
		return userId;
	}

    @DBField
    @XmlElement
    public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getSuperuserId() {
		return superuserId;
	}

    @DBField
    @XmlElement
    public void setSuperuserId(long superuserId) {
		this.superuserId = superuserId;
	}


	public long getEmailReasonId() {
		return emailReasonId;
	}

    @DBField
    @XmlElement
    public void setEmailReasonId(long emailReasonId) {
		this.emailReasonId = emailReasonId;
	}


	public java.sql.Timestamp getSendedAt() {
		return sendedAt;
	}

    @DBField
    @XmlElement
    @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
    public void setSendedAt(java.sql.Timestamp sendedAt) {
		this.sendedAt = sendedAt;
	}


	public String getContent() {
		return content;
	}

    @DBField
    @XmlElement
    public void setContent(String content) {
		this.content = content;
	}


	public String getSubject() {
		return subject;
	}

    @DBField
    @XmlElement
    public void setSubject(String subject) {
		this.subject = subject;
	}

}
