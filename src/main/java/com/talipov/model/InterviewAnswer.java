package com.talipov.model;


import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@DBEntity
@XmlRootElement
public class InterviewAnswer extends Model {

	private long rating;
	private long interviewResultId;
	private long interviewQuestionId;


	public long getRating() {
		return rating;
	}

	@DBField
	@XmlElement
	public void setRating(long rating) {
		this.rating = rating;
	}


	public long getInterviewResultId() {
		return interviewResultId;
	}

	@DBField
	@XmlElement
	public void setInterviewResultId(long interviewResultId) {
		this.interviewResultId = interviewResultId;
	}


	public long getInterviewQuestionId() {
		return interviewQuestionId;
	}

	@DBField
	@XmlElement
	public void setInterviewQuestionId(long interviewQuestionId) {
		this.interviewQuestionId = interviewQuestionId;
	}

}
