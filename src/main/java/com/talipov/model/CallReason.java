package com.talipov.model;

import javax.xml.bind.annotation.*;

import com.talipov.dao.annotation.DBRelation;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.CallDao;

import java.util.ArrayList;
import java.util.List;


@DBEntity
@XmlSeeAlso({Model.class})
@XmlRootElement
public class CallReason extends Model {

	private String name;
	private String script;
	private List<Call> calls = new ArrayList<>();


	public String getName() {
		return name;
	}

	@DBField
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}


	public String getScript() {
		return script;
	}

	@DBField
	@XmlElement
	public void setScript(String script) {
		this.script = script;
	}


	public List<Call> getCalls() {
		return calls;
	}

	@XmlElementWrapper(name = "calls")
	@XmlElement(name = "call")
	@DBRelation(controller = CallDao.class)
	public void setCalls(List<Call> calls) {
		this.calls = calls;
	}

	public String toString() {
		return "CallReason #" + id + " ["
				+ " name = " + name
				+ " script = " + script
				+ " calls.count = " + getCalls().size()
				+ "]";
	}

}
