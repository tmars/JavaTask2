package com.talipov.model;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Марсель on 21.02.2017.
 */
@XmlRootElement
public class Model {
    protected long id;

    public long getId() {
        return id;
    }

    @XmlAttribute(name="id")
    public void setId(long id) {
        this.id = id;
    }
}
