package com.talipov.model;

import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@DBEntity
@XmlRootElement
public class InterviewQuestion extends Model {

	private long interviewId;
	private String question;
	private String criteria;


	public long getInterviewId() {
		return interviewId;
	}

    @DBField
    @XmlElement
    public void setInterviewId(long interviewId) {
		this.interviewId = interviewId;
	}


	public String getQuestion() {
		return question;
	}

    @DBField
    @XmlElement
    public void setQuestion(String question) {
		this.question = question;
	}


	public String getCriteria() {
		return criteria;
	}

    @DBField
    @XmlElement
    public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

}
