package com.talipov.model;


import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.SqlTimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class InterviewResult extends Model {

	private long userId;
	private long superuserId;
	private java.sql.Timestamp createdAt;
	private long totalRating;


	public long getUserId() {
		return userId;
	}

	@DBField
	@XmlElement
	public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getSuperuserId() {
		return superuserId;
	}

	@DBField
	@XmlElement
	public void setSuperuserId(long superuserId) {
		this.superuserId = superuserId;
	}


	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}

	@DBField
	@XmlElement
    @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
    public void setCreatedAt(java.sql.Timestamp createdAt) {
		this.createdAt = createdAt;
	}


	public long getTotalRating() {
		return totalRating;
	}

	@DBField
	@XmlElement
	public void setTotalRating(long totalRating) {
		this.totalRating = totalRating;
	}

}
