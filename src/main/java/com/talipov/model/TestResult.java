package com.talipov.model;

import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.SqlTimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class TestResult extends Model {

	private long userId;
	private long rating;
	private java.sql.Timestamp createdAt;


	public long getUserId() {
		return userId;
	}

    @DBField
    @XmlElement
    public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getRating() {
		return rating;
	}

    @DBField
    @XmlElement
    public void setRating(long rating) {
		this.rating = rating;
	}


	public java.sql.Timestamp getCreatedAt() {
		return createdAt;
	}

    @DBField
    @XmlElement
    @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
    public void setCreatedAt(java.sql.Timestamp createdAt) {
		this.createdAt = createdAt;
	}

}
