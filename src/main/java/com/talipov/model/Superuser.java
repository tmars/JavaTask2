package com.talipov.model;


import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@DBEntity
@XmlRootElement
public class Superuser extends Model {

	private String firstname;
	private String middlename;
	private String lastname;
	private String email;


	public String getFirstname() {
		return firstname;
	}

	@DBField
	@XmlElement
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getMiddlename() {
		return middlename;
	}

    @DBField
    @XmlElement
    public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}


	public String getLastname() {
		return lastname;
	}

    @DBField
    @XmlElement
    public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getEmail() {
		return email;
	}

    @DBField
    @XmlElement
    public void setEmail(String email) {
		this.email = email;
	}

}
