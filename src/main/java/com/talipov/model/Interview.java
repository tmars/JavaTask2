package com.talipov.model;


import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@DBEntity
@XmlRootElement
public class Interview extends Model {

	private String name;


	public String getName() {
		return name;
	}

	@DBField
	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

}
