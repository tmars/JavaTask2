package com.talipov.model;

import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.SqlTimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class ScheduledCall extends Model {

	private long callId;
	private java.sql.Timestamp scheduledAt;


	public long getCallId() {
		return callId;
	}

	@DBField
	@XmlElement
	public void setCallId(long callId) {
		this.callId = callId;
	}


	public java.sql.Timestamp getScheduledAt() {
		return scheduledAt;
	}

	@DBField
	@XmlElement
    @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
    public void setScheduledAt(java.sql.Timestamp scheduledAt) {
		this.scheduledAt = scheduledAt;
	}

}
