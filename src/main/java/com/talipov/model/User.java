package com.talipov.model;

import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.SqlDateAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class User extends Model {

	private long bitrixId;
	private String firstname;
	private String middlename;
	private String lastname;
	private String email;
	private String phone;
	private java.sql.Date birthdate;


	public long getBitrixId() {
		return bitrixId;
	}

    @DBField
    @XmlElement
    public void setBitrixId(long bitrixId) {
		this.bitrixId = bitrixId;
	}


	public String getFirstname() {
		return firstname;
	}

    @DBField
    @XmlElement
    public void setFirstname(String firstname) {
		this.firstname = firstname;
	}


	public String getMiddlename() {
		return middlename;
	}

    @DBField
    @XmlElement
    public void setMiddlename(String middlename) {
		this.middlename = middlename;
	}


	public String getLastname() {
		return lastname;
	}

    @DBField
    @XmlElement
    public void setLastname(String lastname) {
		this.lastname = lastname;
	}


	public String getEmail() {
		return email;
	}

    @DBField
    @XmlElement
    public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}

    @DBField
    @XmlElement
    public void setPhone(String phone) {
		this.phone = phone;
	}


	public java.sql.Date getBirthdate() {
		return birthdate;
	}

    @DBField
    @XmlElement
    @XmlJavaTypeAdapter(SqlDateAdapter.class)
    public void setBirthdate(java.sql.Date birthdate) {
		this.birthdate = birthdate;
	}

}
