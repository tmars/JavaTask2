package com.talipov.model;

import com.talipov.dao.CallDao;
import com.talipov.dao.EmailDao;
import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.DBRelation;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;


@DBEntity
@XmlRootElement
public class EmailReason extends Model {

	private String name;
	private String template;
	private String subject;
    private List<Email> emails = new ArrayList<>();

	@DBField
	@XmlElement
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getTemplate() {
		return template;
	}

	@DBField
	@XmlElement
	public void setTemplate(String template) {
		this.template = template;
	}


	public String getSubject() {
		return subject;
	}

	@DBField
	@XmlElement
	public void setSubject(String subject) {
		this.subject = subject;
	}

    public List<Email> getEmails() {
        return emails;
    }

    @XmlElementWrapper(name = "emails")
    @XmlElement(name = "email")
    @DBRelation(controller = EmailDao.class)
    public void setEmails(List<Email> calls) {
        this.emails = emails;
    }

}
