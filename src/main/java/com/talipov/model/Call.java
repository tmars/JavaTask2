package com.talipov.model;


import com.talipov.dao.annotation.DBField;
import com.talipov.dao.annotation.DBEntity;
import com.talipov.dao.annotation.SqlTimestampAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


@DBEntity
@XmlRootElement
public class Call extends Model {

	private long callReasonId;
	private long userId;
	private long superuserId;
	private java.sql.Timestamp createdAt;
	private long status;


	public long getCallReasonId() {
		return callReasonId;
	}

	@DBField
	@XmlElement
	public void setCallReasonId(long callReasonId) {
		this.callReasonId = callReasonId;
	}

	public long getUserId() {
		return userId;
	}


	@DBField
	@XmlElement
	public void setUserId(long userId) {
		this.userId = userId;
	}


	public long getSuperuserId() {
		return superuserId;
	}

	@DBField
	@XmlElement
	public void setSuperuserId(long superuserId) {
		this.superuserId = superuserId;
	}


	public long getStatus() {
		return status;
	}

	@DBField
	@XmlElement
	public void setStatus(long status) {
		this.status = status;
	}


    public java.sql.Timestamp getCreatedAt() {
        return createdAt;
    }

    @DBField
    @XmlElement
    @XmlJavaTypeAdapter(SqlTimestampAdapter.class)
    public void setCreatedAt(java.sql.Timestamp createdAt) {
        this.createdAt = createdAt;
    }

    public String toString() {
		return "Call #" + id + " ["
				+ " callReasonId = " + callReasonId
				+ " userId = " + userId
				+ " superuserId = " + superuserId
				+ " status = " + status
				+ "]";
	}
}
